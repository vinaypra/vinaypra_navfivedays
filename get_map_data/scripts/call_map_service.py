#!/usr/bin/env python

# Import dependencies
import rospy
from nav_msgs.srv import GetMap, GetMapRequest

# Initialize node
rospy.init_node('call_map_service_node')

#  Block until a service is available
rospy.wait_for_service('/static_map')

# Service Client to call the /static_map service
call_map_service_service = rospy.ServiceProxy('/static_map', GetMap)

# Trigger the request
call_map_service_request = GetMapRequest()

# Get the map data
result = call_map_service_service(call_map_service_request)

# Print the dimensions and resolution of the map in the screen
print(result)