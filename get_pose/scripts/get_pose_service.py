#! /usr/bin/env python

# Import dependencies
import rospy
from std_srvs.srv import Empty, EmptyResponse
from geometry_msgs.msg import PoseWithCovarianceStamped, Pose

# Initialize node
rospy.init_node('pose_service_server_node')

# Create an instance for pose
husky_pose = Pose()

def service_cb(request):
    # Print the Husky robot's pose
    print(husky_pose)

    return EmptyResponse()
    
def callback(msg):
    # Reference the husky_pose variable to store the value of the Husky robot's pose
    global husky_pose

    # Parse the current location
    husky_pose = msg.pose.pose
    
# Create the /get_pose_service service
get_pose_service_service = rospy.Service('/get_pose_service', Empty , service_cb)

# Create the subscriber to parse the Husky robot's pose
get_pose_sub = rospy.Subscriber('/amcl_pose', PoseWithCovarianceStamped, callback)

# Mantain the service open
rospy.spin()