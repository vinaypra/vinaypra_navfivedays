#! /usr/bin/env python

# Import the necessary dependencies
import rospy
from std_srvs.srv import Empty, EmptyRequest

# Initialize the node
rospy.init_node('init_particles_caller_service_client_node')

#  Block until a service is available
rospy.wait_for_service('/global_localization')

# Service Client to call the /global_localization service
init_particles_caller_service = rospy.ServiceProxy('/global_localization', Empty)

# Trigger the request
msg = EmptyRequest()

# Get the particle cloud data
result = init_particles_caller_service(msg)

# Print on the screen
print(result)