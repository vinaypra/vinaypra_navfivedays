#!/usr/bin/env python

# Import ncessary dependencies
import rospy
from geometry_msgs.msg import Twist, PoseWithCovarianceStamped
from std_srvs.srv import Empty, EmptyRequest

class HuskyRobotMoveClass():

    def __init__(self):

        # Defining a publisher for publishing velocity commands
        self.husky_vel_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)

        # Creating an instance of Twist
        self.husky_vel_obj = Twist()

        # Defining a subscriberfor reading pose values
        self.husky_pose_sub = rospy.Subscriber('/amcl_pose', PoseWithCovarianceStamped, self.sub_cb)

        # Create an instance for the request
        self.husky_sub_obj = PoseWithCovarianceStamped()

        # Block until a service is available
        rospy.wait_for_service('/global_localization')

        # Service Client to call the /global_localization service
        self.init_particles_caller_service = rospy.ServiceProxy('/global_localization', Empty)

        # Create an instance for the request
        self.req = EmptyRequest()

        # Initialize for keyboard cancellation input
        self.ctrl_c = False

        # To stop everything in case the cancellation is input from the keyboard
        rospy.on_shutdown(self.shutdownhook)

        # Set the rate
        self.rate = rospy.Rate(10)


    def shutdownhook(self):

        # Stop the Husky robot
        self.husky_stop()

        # Set the variable to True, since cancelled from the keyboard
        self.ctrl_c = True

    def husky_stop(self):

        # Set the linear and angular velocities zero to stop the robot
        self.husky_vel_obj.linear.x = 0.0
        self.husky_vel_obj.angular.z = 0.0

        # Publish the velocity values
        self.i = 0
        for self.i in range(0, 20):
            self.husky_vel_pub.publish(self.husky_vel_obj)
            self.rate.sleep()

    def husky_forward(self):

        # Set the linear and angular velocities zero to move the robot forward
        self.husky_vel_obj.linear.x = 0.5
        self.husky_vel_obj.angular.z = 0.0

        # Publish the velocity values
        self.i = 0
        for self.i in range(0, 50):
            self.husky_vel_pub.publish(self.husky_vel_obj)
            self.rate.sleep()

    def husky_turn(self):

        # Set the linear and angular velocities zero to turn the robot
        self.husky_vel_obj.linear.x = 0.0
        self.husky_vel_obj.angular.z = 0.8

        # Publish the velocity values
        self.i = 0
        for self.i in range(0, 25):
            self.husky_vel_pub.publish(self.husky_vel_obj)
            self.rate.sleep()

    def move_square(self):

        self.i = 0
        while not self.ctrl_c and self.i in range(0, 4):

            self.husky_forward()

            self.husky_stop()

            self.husky_turn()

            self.husky_stop()

        self.husky_stop()

    def call_service(self):
        result = self.init_particles_caller_service(self.req)

    def sub_cb(self, msg):
        self.husky_sub_obj = msg

    def compute_covar(self):

        # Parse x, y and z covariance values from the matrix in image
        covar_x = self.husky_sub_obj.pose.covariance[0]
        covar_y = self.husky_sub_obj.pose.covariance[7]
        covar_z = self.husky_sub_obj.pose.covariance[35]

        # Compute covariance
        covar_var = (covar_x+covar_y+covar_z)/3
        return covar_var

def main():
    try:
        # Initialize the node
        rospy.init_node('square_move_node', anonymous=True)

        # Create an instance of the class
        HuskyRobotMoveClass_object = HuskyRobotMoveClass()

        # Set the initial value to enter while loop
        covar_var = 1

        while covar_var > 0.65:
            HuskyRobotMoveClass_object.call_service()
            HuskyRobotMoveClass_object.move_square()
            covar_var = HuskyRobotMoveClass_object.compute_covar()

            # Compare the computed value of covariance with 0.65
            if covar_var > 0.65:
                rospy.loginfo("Covariance > 0.65")
            else:
                rospy.loginfo("Covariance < 0.65")

    except rospy.ROSInterruptException:
        rospy.loginfo('Something went wrong!')
        #pass

if __name__ == '__main__':

    # Call main function
    main()
