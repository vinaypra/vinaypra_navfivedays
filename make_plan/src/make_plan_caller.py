#! /usr/bin/env python

# Include necessary dependencies
import rospy
from nav_msgs.srv import GetPlan, GetPlanRequest
 
# Initialize the node
rospy.init_node('make_plan_caller_node')

# Wait for the service  to be running
rospy.wait_for_service('/move_base/make_plan')

# Create the connection to the service
make_plan_caller_service = rospy.ServiceProxy('/move_base/make_plan', GetPlan)

# Create an instance of type GetPlanRequest
req = GetPlanRequest()

# Set the start header frame id
req.start.header.frame_id = 'map'

# Start pose
req.start.pose.position.x = 0.000
req.start.pose.position.y = 0.000
req.start.pose.position.z = 0.000
req.start.pose.orientation.x = 0.0
req.start.pose.orientation.y = 0.0
req.start.pose.orientation.z = 0.0
req.start.pose.orientation.w = 0.0

# Set the goal header frame id
req.goal.header.frame_id = 'map'

# Goal pose
req.goal.pose.position.x = -2.210
req.goal.pose.position.y = -2.568
req.goal.pose.position.z = 0.000
req.goal.pose.orientation.x = 0.0
req.goal.pose.orientation.y = 0.0
req.goal.pose.orientation.z = 0.0
req.goal.pose.orientation.w = 1.0

# Get the result from the service
result = make_plan_service(req)

# Print the result from the service
print(result)