#! /usr/bin/env python

# Include necessary dependencies
import rospy
import time
import actionlib
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseWithCovarianceStamped
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal, MoveBaseResult, MoveBaseFeedback
import os
import rosparam
import time

class SendGoals(object):
    def __init__(self):
        
        # Establish connection to the action server
        client = actionlib.SimpleActionClient('/move_base', MoveBaseAction)

        #Set the rate
        rate = rospy.Rate(1)

        # Create goal instance
        goal=MoveBaseGoal()

        # Create goal_tmp instance
        goal_tmp = Pose()
        
        self._ctrl_c = False
        rospy.on_shutdown(self.shutdownhook)

        os.chdir("/home/user/catkin_ws/src/navigation_exam/params")
        paramlist=rosparam.load_file("points.yaml",default_namespace=None)

        for params,ns in paramlist:
            rosparam.upload_params(ns,params) #ns,param
        
        while not self._ctrl_c:

            for i in range(0,2):
                
                if i==0:
                    # Pass the label for parsing
                    self.tag = "point1"

                elif i==1:
                    # Pass the label for parsing
                    self.tag = "point2"
            
                # Parsing the position and orientation co-ordinates
                goal_tmp.position.x=rosparam.get_param(self.tag+'/position/x')
                goal_tmp.position.y=rosparam.get_param(self.tag+'/position/y')
                goal_tmp.position.z=rosparam.get_param(self.tag+'/position/z')
                goal_tmp.orientation.x=rosparam.get_param(self.tag+'/orientation/x')
                goal_tmp.orientation.y=rosparam.get_param(self.tag+'/orientation/y')
                goal_tmp.orientation.z=rosparam.get_param(self.tag+'/orientation/z')
                goal_tmp.orientation.w=rosparam.get_param(self.tag+'/orientation/w')
                goal.target_pose.pose=goal_tmp
                goal.target_pose.header.frame_id='map'
            
                # Wait till server is up
                client.wait_for_server()

                # Send the goal
                client.send_goal(goal, feedback_cb=self.callback)
                # Wait for the result
                client.wait_for_result()

            # Get the client state in case of error for debugging
            result=client.get_state()
                
            # Print the result
            if result==3:
                print('Successful')
                self.shutdownhook()
                
            
    def shutdownhook(self):
            
        rospy.loginfo("Shutdown time!")
        self._ctrl_c = True
        
    def callback(self, data):
        return



if __name__ == "__main__":

    # Initialize node
    rospy.init_node('send_goals_node')
    send_goals_object_one = SendGoals()
    #send_goals_object_one = SendGoals("point1")
    #send_goals_object_two = SendGoals("point2")
    rospy.spin() # mantain the service open.