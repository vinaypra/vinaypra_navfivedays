#! /usr/bin/env python

# Include necessary dependencies
import rospy
import time
import actionlib
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal, MoveBaseResult, MoveBaseFeedback

def feedback_callback(feedback):
    
    print('Towards goal')

# Initialize the node
rospy.init_node('send_goal_client_node')

# Establish connection to the action server
client = actionlib.SimpleActionClient('/move_base', MoveBaseAction)

# Wait for the action server
client.wait_for_server()

# Create an instance for the goal for the action server
goal = MoveBaseGoal()

# Set the target pose frame id
goal.target_pose.header.frame_id = 'map'

for i in range(0,3):
    if i==0:
        # Set the Target pose values
        goal.target_pose.pose.position.x = -2.210
        goal.target_pose.pose.position.y = -2.568
        goal.target_pose.pose.position.z = 0.0
        goal.target_pose.pose.orientation.w = 1.0
        print('1')
        # Send the goal to the action server
        client.send_goal(goal, feedback_cb=feedback_callback)

        # Wait for the result
        client.wait_for_result()

    elif i==1:
        goal.target_pose.pose.position.x = 0.726
        goal.target_pose.pose.position.y = -2.918
        goal.target_pose.pose.position.z = 0.0
        goal.target_pose.pose.orientation.w = 1.0
        print('2')
        # Send the goal to the action server
        client.send_goal(goal, feedback_cb=feedback_callback)

        # Wait for the result
        client.wait_for_result()

    elif i==2:
        goal.target_pose.pose.position.x = 1.556
        goal.target_pose.pose.position.y = 2.148
        goal.target_pose.pose.position.z = 0.0
        goal.target_pose.pose.orientation.w = 1.0
        print('3')
        # Send the goal to the action server
        client.send_goal(goal, feedback_cb=feedback_callback)

        # Wait for the result
        client.wait_for_result()
        '''
        goal.target_pose.pose.position.x = 0.330
        goal.target_pose.pose.position.y = 0.017
        goal.target_pose.pose.position.z = 0.0
        goal.target_pose.pose.orientation.w = 1.0
        '''
'''
# Send the goal to the action server
client.send_goal(goal, feedback_cb=feedback_callback)

# Wait for the result
client.wait_for_result()
'''